describe('protractor lab a starter', () => {

  it('should get Google URL', () => {
    browser.waitForAngularEnabled(false);
    browser.get('http://www.google.com');

    // Validate the title is "Google"
    expect(browser.getTitle()).toEqual('Google');
  });

});
