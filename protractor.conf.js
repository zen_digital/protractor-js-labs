exports.config = {
  directConnect: true,
  // seleniumAddress: 'http://localhost:4444/wd/hub',
  framework: 'jasmine',
  specs: ['./e2e/**/*.spec.js'],
  // Options to be passed to Jasmine.
  jasmineNodeOpts: {
    defaultTimeoutInterval: 10000
  },
  // multiCapabilities: [
  //   {
  //     browserName: 'firefox'
  //   },
  //   {
  //     browserName: 'chrome'
  //   }
  // ]
};
